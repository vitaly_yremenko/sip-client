# Эксперемент с SIP и WebRTC

Задача: https://positrace.jira.com/browse/CT-576


#Настройка сервера
Сервер Ubuntu

```bash

    wget -O - https://files.freeswitch.org/repo/ubuntu-1604/freeswitch-unstable/freeswitch_archive_g0.pub | apt-key add -
    echo "deb http://files.freeswitch.org/repo/ubuntu-1604/freeswitch-unstable/ xenial main" > /etc/apt/sources.list.d/freeswitch.list

    apt-get update && apt-get install -y freeswitch-meta-vanilla git nginx

    
    #remove ipv6 configs
    cd /etc/freeswitch/sip_profiles/
    mv internal-ipv6.xml internal-ipv6.xml.inactive
    mv external-ipv6 external-ipv6.xml.inactive
    
    #генерируем сертификаты
    cd /usr/src/
    wget http://files.freeswitch.org/downloads/ssl.ca-0.1.tar.gz
    tar zxfv ssl.ca-0.1.tar.gz
    cd ssl.ca-0.1/
    perl -i -pe 's/md5/sha256/g' *.sh
    perl -i -pe 's/1024/4096/g' *.sh
    ./new-root-ca.sh
    ./new-server-cert.sh 46.101.212.205  # ip сервера или домен
    ./sign-server-cert.sh 46.101.212.205  # ip сервера или домен
    mkdir /etc/freeswitch/certs
    cat 46.101.212.205.crt 46.101.212.205.key > /etc/freeswitch/certs/wss.pem
    cat 46.101.212.205.crt 46.101.212.205.key > /etc/freeswitch/certs/agent.pem
    cat 46.101.212.205.crt 46.101.212.205.key > /etc/freeswitch/certs/cafile.pem
    
```


Edit  /etc/nginx/sites-available/default
```bash

    server {
    	listen 80 default_server;
    	listen [::]:80 default_server;
    	listen 443 ssl;
    	root /var/www/html;
    	index index.html index.htm index.nginx-debian.html;
    	server_name _;
    	ssl_certificate /etc/freeswitch/certs/wss.pem;
        ssl_certificate_key /etc/freeswitch/certs/wss.pem;
    	location / {
    		try_files $uri $uri/ =404;
    	}
    }

```


Исправляем файлы которые лежат в папки *server*


```bash
    service nginx restart
    service freeswitch restart
    service freeswitch status
    
```

Копировать содержимое из папки simpleexample/www на сервер в /var/www/html

Должно завестись.


#Сборка Cordova приложения

```bash
    git clone git@bitbucket.org:vitaly_yremenko/sip-client.git sip-client
    cd sip-client
    
    npm install -g cordova
    npm install
    npm run build
    cd simpleexample
    cordova platform add android
    cordova run android --device

```




