
require('file-loader?name=index.html!./../html/index.html');

require('file-loader?name=avatar.png!./../img/rachel.png');

require 'semantic-ui-css/semantic.css'
require './../css/index.css'

global.SIP = SIP = require 'sip.js'
$ = require 'jquery'
require 'semantic-ui-css/semantic'

serializeForm = ($form) ->
  o = {}
  $.each $($form).serializeArray(), (_, k) -> o[k.name] = k.value
  o

global.CONFIG = {
  server: null
}



global.UA = null
global.session = null

showError = (message, title = '', type='error') ->
  showMessage(message, title, type)

showMessage = (message, title = '', type='success') ->
  $('.f-message').off().remove()
  $el = $("""
  <div class="ui floating #{type} message f-message" >
    <i class="close icon"></i>
      <div class="header">#{title}</div>
      <p>#{message}</p>
    </div>
  """)

  $('body').append $el
  $el.on 'click', -> $el.transition('fade').remove()



showSection = (name) ->
  $('section').addClass('hidden')
  $("##{name}").removeClass('hidden')

onInvite = (session) ->
  options = {
    media: {
      constraints: {audio:true, vidio:false}
    }
  };
  session.accept(options);
  global.callsession = session
  session.on 'terminated', -> showSection 'call'
  showSection 'call'





onMessage = -> console.log 'message', arguments

onRegistred = ->
  saveConfig()
  $('.js-user-name').text(CONFIG.server.uri)
  showSection 'menu'

onUnregistred = ->
  CONFIG.server = null
  saveConfig()
  showSection('login');

callToUser = (number, callback) ->


window.addEventListener( "deviceready", onDeviceReady, false );

onDeviceReady = ->
  console.log 'resdy'
  audioinput.start {
    streamToWebAudio: true
  }
  undefined


call = ->
  $section = $('#call')
  $('.js-cancel-call',$section).on 'click', (e)->
    e.preventDefault()
    global.callsession.bye()
    showSection 'menu'

getInviteConfiguration = () ->
  if true
    getInviteConfigurationCordova()
  else
    getInviteConfigurationBrowser()

getInviteConfigurationBrowser = ->
  {
    media:{
      constraints: {audio: true, video: false}
      render: {
        remote: document.getElementById('remoteVideo'),
        local: document.getElementById('localVideo')
      }
    }
  }

getInviteConfigurationCordova = ->

  audioContext = new (window.AudioContext || window.webkitAudioContext)();

  calloptions = {
    media:{
      stream: (new (window.AudioContext || window.webkitAudioContext)()).createMediaStreamDestination().stream
      constraints: {audio: true, video: false}
      render: {
        remote: document.getElementById('remoteVideo'),
        local: document.getElementById('localVideo')
      }
    }
  }




menu = ->
  $calbar = $('.call-bar')
  $cbr = $('.call-bar__number-text')
  $calbarItems = $('.call-bar__pad-item')
  $clear = $('.call-bar__number-clear')
  isCallToUser = false

  $clear.on 'click', (e) ->
    e.preventDefault()
    text = $cbr.text()
    text = text.split('').slice(0, text.length-1).join('')
    $cbr.text(text)

  $calbarItems.on 'click', (e) ->
    e.preventDefault()
    text = $cbr.text()
    key = $(@).data('key')
    if key == 'message'
      return  showError('Enter number') unless !!text
      message = prompt('text')
      context = UA.message(text, message);
      context.on 'progress', -> showMessage 'progress'
      context.on 'accepted', -> showMessage 'accepted'
      context.on 'rejected', -> showError 'rejected'
      context.on 'failed', -> showError 'failed'

    else if key == 'call'
      return showError('Enter number') unless !!text
      return showError('You already call') if isCallToUser
      isCallToUser = true

      options = {
        media: {
          constraints: {
            audio: true,
            video: false
          },
          render: {
            remote: document.getElementById('remoteVideo'),
            local: document.getElementById('localVideo')
          }
        }
      }
      global.callsession= session = UA.invite(text, options);
      session.on 'progress', ->  showMessage 'progress'
      session.on 'accepted', -> showSection('call')
      session.on 'rejected', -> isCallToUser = false; showError 'rejected'
      session.on 'failed', -> isCallToUser = false; showError 'failed'
      session.on 'terminated', ->isCallToUser = false; showError 'terminated'
      session.on 'cancel', -> isCallToUser = false; showError 'cancel'

    else
      $cbr.text(text + key)



onSignOut = ->  global.UA.unregister()

restoreConfig = ->
  raw = localStorage.getItem('sip-config')
  global.CONFIG = JSON.parse(raw) if raw

saveConfig = ->
  s = JSON.stringify(global.CONFIG)
  localStorage.setItem('sip-config', s)

doLogin = ->
  global.UA = new SIP.UA(CONFIG.server)
  global.UA.on 'connecting', -> form.addClass 'loading'
  global.UA.on 'disconnected', -> showSection('login');   form.removeClass('loading')
  global.UA.on 'connected', -> showSection('menu')
  global.UA.on 'registered', (incomingResponse)-> onRegistred incomingResponse
  global.UA.on 'unregistered', -> onUnregistred()
  global.UA.on 'registrationFailed', ->
    console.log('registrationFailed',arguments)
    form.removeClass('loading'); showSection('login')
  global.UA.on 'invite', onInvite
  global.UA.on 'message', onMessage

login = ->
  $('#button-connect').on 'click', (e)->
    e.preventDefault()
    data = serializeForm('#form-login')
    form = $("#form-login")
    $("#form-login *[name='user']").parent('.field').toggleClass('error', !data.user)
    $("#form-login *[name='password']").parent('.field').toggleClass('error', !data.password)
    $("#form-login *[name='server']").parent('.field').toggleClass('error', !data.server)

    if $("#form-login .error").length
      console.log 'error'
    else
      ip = (/([0-9.]+)/).exec(data.server)[0]
      CONFIG.server = {
        ip: ip
        uri: "#{data.user}@#{ip}"
        ws_servers: data.server
        authorizationUser: data.user
        password: data.password
        register:true,
        hackIpInContact: true,
        hackViaTcp: false,
      }

      doLogin()


$ ->

  cordova?.plugins?.certificates?.trustUnsecureCerts(true);

  restoreConfig()
  menu()
  login()
  call()

  doLogin() if CONFIG.server

  $('.js-signout').on 'click', onSignOut




