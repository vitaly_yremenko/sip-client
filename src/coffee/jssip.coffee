require('file-loader?name=index.html!./../html/index.html');

require('file-loader?name=avatar.png!./../img/rachel.png');

require 'semantic-ui-css/semantic.css'
require './../css/index.css'

global.SIP = require 'sip.js'
global.JsSIP = require 'jssip'
$ = require 'jquery'
require 'semantic-ui-css/semantic'



global.start = (user1 = '1001', user2 = '1002') ->

  socket = new JsSIP.WebSocketInterface('wss://46.101.212.205:7443')
  configuration = {
    sockets  : [ socket ],
    uri      :  user1 + '@46.101.212.205',
    password : '123456789',
    session_timers: true
    no_answer_timeout: 10000
    session_timers_use_update: 10000

  }

  onNewSession = (data) ->
    console.log('newRTCSession', data)
    onIvite(data.session);
    if data.originator == 'remote'
      data.session.answer({
        mediaConstraints: { 'audio': true, 'video': false }
      })


  onIvite = (session) ->
    session.on 'peerconnection', -> console.log 'peerconnection', arguments
    session.on 'sending', -> console.log 'sending', arguments
    session.on 'connecting', -> console.log 'connecting', arguments
    session.on 'progress', -> console.log 'progress', arguments
    session.on 'accepted', -> console.log 'accepted', arguments
    session.on 'confirmed', -> console.log 'confirmed', arguments
    session.on 'ended', -> console.log 'ended', arguments
    session.on 'failed', -> console.log 'failed', arguments
    session.on 'getusermediafailed', -> console.log 'getusermediafailed', arguments
    session.on 'peerconnection:createofferfailed', -> console.log 'peerconnection:createofferfailed', arguments
    session.on 'peerconnection:createanswerfailed', -> console.log 'peerconnection:createanswerfailed', arguments
    session.on 'peerconnection:setlocaldescriptionfailed', -> console.log 'peerconnection:setlocaldescriptionfailed', arguments
    session.on 'peerconnection:setremotedescriptionfailed', -> console.log 'peerconnection:setremotedescriptionfailed', arguments


  onRegistered = (e) ->
    console.log('registered', e)
    doCall(user2)


  doCall = (user) ->
    eventHandlers = {
      'progress': (e) -> console.log('call is in progress')
      'failed': (e) -> console.log('call failed with cause: ', e.cause)
      'ended':  (e) ->  console.log('call ended with cause: ', e.cause)
      'confirmed':  (e) -> console.log('call confirmed')
    }

    options = {
      'eventHandlers'    : eventHandlers,
      'mediaConstraints' : { 'audio': true, 'video': false }
      'sessionTimersExpires': 10000
    }

    session = coolPhone.call(user, options)



  coolPhone = new JsSIP.UA(configuration)
  coolPhone.on 'connected', (e) -> console.log('connected')
  coolPhone.on 'disconnected', (e) ->  console.log('disconnected')
  coolPhone.on 'newRTCSession', onNewSession
  coolPhone.on 'registered', onRegistered
  coolPhone.on 'unregistered', (e) ->console.log('unregistered', e)
  coolPhone.on 'registrationFailed', (e) -> console.log('registrationFailed', e)

  coolPhone.start()
  coolPhone.register();
