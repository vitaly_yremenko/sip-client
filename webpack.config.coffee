webpack = require('webpack');
path = require("path");
ExtractTextPlugin = require('extract-text-webpack-plugin');
extractCSS = new ExtractTextPlugin('app.css');

module.exports = {
  context: path.join(__dirname, 'src'),
  entry: path.resolve(__dirname, 'src/coffee/app.coffee'),
  resolve:
    extensions: ['.coffee', '.js', '.css']
  output:
    path: path.join(__dirname, 'sipexample/www')
    filename: 'app.js'
  module:
    loaders: [
      {test: /\.coffee$/, loader: "coffee-loader"}
      {test: /\.css$/, loader: extractCSS.extract("css-loader")}
      {test: /\.png$/, loader: "file-loader?prefix=img/&limit=5000"},
      {test: /\.jpg$/, loader: "file-loader?prefix=img/&limit=5000"},
      {test: /\.gif$/, loader: "file-loader?prefix=img/&limit=5000"},
      {test: /\.woff$/, loader: "file-loader?prefix=font/&limit=5000"},
      {test: /\.woff2$/, loader: "file-loader?prefix=font/&limit=5000"},
      {test: /\.eot$/, loader: "file-loader?prefix=font/"},
      {test: /\.ttf$/, loader: "file-loader?prefix=font/"},
      {test: /\.svg$/, loader: "file-loader?prefix=font/"},
    ]
  plugins: [
    extractCSS,
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
#      _: 'underscore',
    })

  ]

}
